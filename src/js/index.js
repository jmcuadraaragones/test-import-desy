console.log('Desy loaded');

/* c-dropdown */

const modules = document.querySelectorAll('[data-module]');
for (var item in modules) if (modules.hasOwnProperty(item)) {
  var moduleValue = modules[item].getAttribute('data-module');
  if (moduleValue == 'c-dropdown'){
    var button = modules[item].querySelector('[data-module = "c-dropdown-button"]');
    var tooltip = modules[item].querySelector('[data-module = "c-dropdown-tooltip"]');
    if(button && tooltip) {
      const hideOnEsc = {
        /* https://atomiks.github.io/tippyjs/v6/plugins/#hideonesc */
        name: 'hideOnEsc',
        defaultValue: true,
        fn({hide}) {
          function onKeyDown(event) {
            if (event.keyCode === 27) {
              hide();
            }
          }

          return {
            onShow() {
              document.addEventListener('keydown', onKeyDown);
            },
            onHide() {
              document.removeEventListener('keydown', onKeyDown);
            },
          };
        },
      };

      // https://atomiks.github.io/tippyjs/v6/all-props/
      tippy(button, {
        placement: 'bottom-start',
        inlinePositioning: true,
        content: tooltip,
        allowHTML: true, // Make sure you are sanitizing any user data if rendering HTML to prevent XSS attacks.
        trigger: 'click',
        hideOnClick: true,
        interactive: true,
        arrow: false,
        offset: [0, -10],
        theme: '',
        plugins: [hideOnEsc],
        aria: {
          content: 'describedby',
        }
      });
    }
  }
}


exports.printMsg = function() {
  console.log('Desy package loaded');
}
