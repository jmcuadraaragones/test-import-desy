module.exports = {
  /* DESY AND this project's files to be purged with PurgeCSS in npm run prod */
  files: ['./node_modules/desy-frontend/src/**/*.html',
          './node_modules/desy-frontend/src/**/*.njk',
          './src/**/*.html',
          './src/**/*.njk',
          './docs/**/*.html',
          './docs/**/*.njk',]
}
