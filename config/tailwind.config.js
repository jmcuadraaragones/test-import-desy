/* Load Desy tailwindcss config */
const defaultConfig = require('../node_modules/desy-frontend/config/tailwind.config.js')
/* Load PurgeCSS files to add DESY AND this project's files */
const purgeConfig = require('./purge.config')

module.exports = {
  /* Use the Desy default config */
  ...defaultConfig,
  /* Change PurgeCSS files to add DESY AND this project's files */
  purge: purgeConfig.files
}
