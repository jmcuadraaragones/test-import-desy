# README #

This is a test playground project to add desy-frontend as npm dependency in a project. 
This project has a similar configuration as desy-frontend:
* Compiles tailwindcss styles, with a configuration inherited from desy-frontend but adding this specific project files to be purged in css compilation.
* Uses desy-frontentd components and compiles its css styles alongside with this project's css styles.
* Allows this project to import nunjucks from desy-frontend components.

### How do I get set up? ###

* Configuration:
Run `npm install` first.
Use `npm run dev` to generate CSS from `src/styles.css` to `/build/styles.css`, compile html and .njk files, listen to changes in njk, css and html, and browser-sync reload.
Use `npm run prod` to generate CSS and Purge it and Minify it.
* Dependencies:
desy-frontend
Node.js v12.18.3, Tailwind CSS, AutoPrefixer, PurgeCSS and CSSnano configed in PostCSS